# Внешние библиотеки
import asyncio
from loguru import logger
from aiogram import Bot, Dispatcher
import warnings

from src import config_data
from src.external_services.updates_receiver import use_polling, use_webhook
from src.handlers import public
from src.inference.predictor_prod import CategoryPredictor
from src.data_wranglers.nutrition_matcher import NutritionMatcher

warnings.filterwarnings("ignore", category=UserWarning)
# Токен бота
TOKEN = config_data.config.bot_token.get_secret_value()

# Настройки webhook
USE_WEBHOOK = int(config_data.config.use_webhook.get_secret_value())
WEB_SERVER_HOST = config_data.config.web_server_host.get_secret_value()
WEB_SERVER_PORT = int(config_data.config.web_server_port.get_secret_value())
WEBHOOK_PATH = config_data.config.webhook_path.get_secret_value()
BASE_WEBHOOK_URL = config_data.config.base_webhook_url.get_secret_value()


MODEL_PATH = './models_prod/X_101_32x8d_FPN_3x_batch30/traced_model.pt'
DB_PATH = './data/bot/calories.parquet'

logger.add("data/bot/logs/{time:DD:MM:YY}.log",  # Файл для записи логов
           enqueue=True,  # Поддержка асинхронности
           rotation="1 week",  # Новый файл каждую неделю
           retention="1 month")  # Удалять файлы через месяц


# Действия при старте вебхук сервера
async def on_startup(bot: Bot) -> None:
    await bot.set_webhook(f"{BASE_WEBHOOK_URL}{WEBHOOK_PATH}")


def main() -> None:
    predictor = CategoryPredictor(MODEL_PATH)
    matcher = NutritionMatcher(DB_PATH)
    bot = Bot(token=TOKEN, parse_mode="markdown")
    dp = Dispatcher(bot=bot,
                    logger=logger,
                    predictor=predictor,
                    matcher=matcher)
    # Добавляем роутеры в диспетчер
    dp.include_routers(public.router)

    # Выбор способа получения апдейтов
    if USE_WEBHOOK:
        use_webhook(bot, dp, on_startup, WEB_SERVER_HOST,
                    WEB_SERVER_PORT, WEBHOOK_PATH)
        pass
    else:
        logger.info('Start polling')
        asyncio.run(use_polling(bot, dp))


if __name__ == "__main__":
    main()
