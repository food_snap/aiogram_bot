import pandas as pd
from typing import Any


class NutritionMatcher():
    """
    A class for matching nutrition information.
    """

    def __init__(self, parquet_path: str) -> None:
        """
        Initialize the NutritionMatcher object.

        Parameters:
        parquet_path (str): The path to the parquet file.

        Returns:
        None
        """
        df = pd.read_parquet(parquet_path)
        self.db = self.create_db(df)

    def create_db(self, df: pd.DataFrame) -> dict:
        """
        Create a database from the given DataFrame.

        Parameters:
        df (pd.DataFrame): The DataFrame to create the database from.

        Returns:
        dict: The created database.
        """
        db = {}
        for i in df.index:
            db[df.id[i]] = {}
            for col in df.columns:
                if col not in ['id']:
                    db[df.id[i]][col] = df[col][i]
        return db

    async def get_nutrition(self, category_id, portion_g=None) -> dict[str, Any]:
        """
        Get the calories for the given category and portion.

        Parameters:
        category_id (str): The id of the category.
        portion_g (float): The portion in grams.

        Returns:
        dict[str, Any]: The nutrition information.
        """
        f = self.db[category_id]
        portion_g = portion_g if portion_g else f['portion_g']
        return {
            'Наименование': f['name_ru'],
            'Калории': round(portion_g / 100 * f['calories_ccal'], 2),
            'Белки': round(portion_g / 100 * f['protein_g'], 2),
            'Жиры': round(portion_g / 100 * f['fat_g'], 2),
            'Углеводы': round(portion_g / 100 * f['carbohydrate_g'], 2),
            }

    async def get_nutrition_report(self, prediction: dict) -> list:
        """
    This function generates a nutrition report based on the prediction dictionary.

    Parameters:
    prediction (dict): A dict where keys are food items and values are their scores.

    Returns:
    list: A list of strings where each string is a formatted report for a food item.
    The report includes the score of the food item and its nutrition information.
    The last element of the list is the total sum of calories in all food items.

    Raises:
    None

    Notes:
    - The nutrition information is obtained by calling the 'get_calories' method.
    - The nutrition information is formatted using 'pprint.pformat'.
    - The report is a list of strings,
    where each string is a formatted report for a food item.
    - The score of the food item is included in the report.
    - The total sum of calories in all food items is included in the report.
    """

        calories_sum = 0
        report = []
        for pred in list(prediction.items()):
            nutrition = await self.get_nutrition(pred[0])
            calories_sum = calories_sum + nutrition['Калории']
            report.append(f"SCORE:{pred[1]}\n{nutrition}\n")
        calories_sum = round(calories_sum, 2)
        return (f"```Калории:{calories_sum}\n{''.join(report)}```")
