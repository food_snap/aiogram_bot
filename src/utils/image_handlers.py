from loguru import logger


async def download_photo(bot,
                         message,
                         dir='data/bot/user_photos/') -> str:
    """Saves photo sent by user to a local file and returns it's path

    Args:
        bot: bot instance
        message: current update
        dir: Path to save photo. Defaults to 'data/user_photos/'.

    Returns:
        Saved photo's filename.
    """
    photo = message.photo[-1]  # get the highest resolution photo
    file_id = photo.file_id

    try:
        file = await bot.get_file(file_id)
    except Exception as ex:
        logger.warning(ex)

    file_path = file.file_path
    image_path = f'{dir}{file_id}.jpg'

    try:
        # download the photo
        await bot.download_file(file_path,
                                destination=image_path)
        return image_path

    except Exception as ex:
        logger.warning(ex)
