from aiogram.filters import Command
from aiogram.filters.text import Text
from aiogram import Router, Bot
from aiogram.types import Message
from aiogram import F
from loguru import logger

from ..keyboards.main_kb import get_main_kb_public
from ..utils.image_handlers import download_photo


router = Router()


@router.message(Command("start"))
async def cmd_start_public(message: Message) -> None:
    """Обработчик команды /start"""
    await message.answer("Выберите действие", reply_markup=get_main_kb_public())


@router.message(Text('Анализ фото'))
async def analyse_photo(message: Message) -> None:
    """Пояснение как анализировать фото"""
    await message.answer("Отправьте фото для анализа и мы предскажем его калорийность",
                         reply_markup=get_main_kb_public())


@router.message(Text('Обратная связь'))
async def feedback(message: Message) -> None:
    '''Фиксирование отзывов пользователей'''
    await message.answer('Можете написать свои отзывы и пожелания прямо здесь',
                         reply_markup=get_main_kb_public())


@router.message(F.photo)
async def handle_photo(message: Message,
                       bot: Bot,
                       predictor,
                       matcher) -> None:
    '''Сохраняем полученное фото и определяем пищевую ценность'''
    image = await download_photo(bot=bot, message=message)
    logger.info(f'Downloaded image {image}')

    prediction = predictor.get_prediction(image)
    nutrition_report = await matcher.get_nutrition_report(prediction)
    await message.answer(nutrition_report)


@router.message(F.text)
async def log_messages(message: Message) -> None:
    '''Логируем сообщения пользователей'''
    logger.info(f'User {message.from_user.id} left message:\n{message.text}')
