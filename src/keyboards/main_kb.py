from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
from aiogram.utils.keyboard import ReplyKeyboardBuilder


def get_main_kb_public() -> ReplyKeyboardMarkup:
    """Возвращает клавиатуру главного меню для незарегистрированных пользователей """
    builder = ReplyKeyboardBuilder()
    buttons = [
        KeyboardButton(text='Анализ фото'),
        KeyboardButton(text='Обратная связь')
    ]

    builder.add(*buttons)
    builder.adjust(1)
    return builder.as_markup(resize_keyboard=True, one_time_keyboard=True)
