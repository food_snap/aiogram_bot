from aiohttp import web
from aiogram.webhook.aiohttp_server import SimpleRequestHandler, setup_application
from aiogram import Bot, Dispatcher
from types import FunctionType


async def use_polling(bot: Bot,
                      dp: Dispatcher) -> None:
    """Удаляет вебхук на ТГ-сервере и запускает поллинг

    :param bot: объект Бота
    :param dp: объект диспетчера (основного роутера)

    """
    await bot.delete_webhook(drop_pending_updates=True)  # удаляет вебхук на ТГ-сервере
    await dp.start_polling(bot, polling_timeout=15)


def use_webhook(bot: Bot,
                dp: Dispatcher,
                on_startup: FunctionType,
                server_host: str,
                server_port: str,
                webhook_path: str) -> None:
    """Регистрирует вебхук на ТГ сервере и запускает web-сервер

    :param bot: объект Бота
    :param dp: объект диспетчера (основного роутера)
    :param on_startup: функция, выполняемая при старте
    :param server_host: вебхук хост
    :param server_port: вебхук порт
    :param webhook_path: путь вебхука
    """
    dp.startup.register(on_startup)
    app = web.Application()
    webhook_requests_handler = SimpleRequestHandler(dispatcher=dp, bot=bot)

    # Регистрируем вебхук в приложении
    webhook_requests_handler.register(app, path=webhook_path)

    # Монтируем диспетчер и бот в приложении
    setup_application(app, dp, bot=bot)

    # Стартуем веб-сервер
    web.run_app(app, host=server_host, port=server_port)
