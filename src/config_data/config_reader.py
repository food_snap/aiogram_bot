from pydantic import BaseSettings, SecretStr, SecretBytes


class Settings(BaseSettings):
    # Желательно вместо str использовать SecretStr
    # для конфиденциальных данных, например, токена бота

    bot_token: SecretStr
    use_webhook: SecretBytes
    web_server_host: SecretStr
    web_server_port: SecretBytes
    webhook_path: SecretStr
    base_webhook_url: SecretStr

    # Вложенный класс с дополнительными указаниями для настроек
    class Config:
        # Имя файла, откуда будут прочитаны данные
        # (относительно текущей рабочей директории)
        env_file = '.env'
        # Кодировка читаемого файла
        env_file_encoding = 'utf-8'


# При импорте файла сразу создастся
# и провалидируется объект конфига,
# который можно далее импортировать из разных мест
config = Settings()
